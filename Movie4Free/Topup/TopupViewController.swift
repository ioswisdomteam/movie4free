//
//  TopupViewController.swift
//  Movie4Free
//
//  Created by BURIN TECHAMA on 3/28/2559 BE.
//  Copyright © 2559 JonGT. All rights reserved.
//

import UIKit

class TopupViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var topupTable: UITableView!
    var topupList:[String] = ["ทรูมันนี่","บัตรเงินสด One2Call"]
    var topupListImage:[String] = ["topup_true","topup_o2c"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:UITableViewCell = self.topupTable.dequeueReusableCellWithIdentifier("cell")! as UITableViewCell
        
        cell.textLabel?.text = topupList[indexPath.row]
        //cell.textLabel?.textColor = UIColor.whiteColor()
        cell.imageView?.image = UIImage(named: topupListImage[indexPath.row])
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return topupList.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("showForm", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showForm") {
            let destinationVC:TopupFormViewController = segue.destinationViewController as! TopupFormViewController
            let selectedIndex = self.topupTable.indexPathForSelectedRow
            destinationVC.topup_method = "เติมเงินด้วย \(topupList[selectedIndex!.row])"
            destinationVC.topup_method_img = topupListImage[selectedIndex!.row]
        }
    }
    
    @IBAction func backPressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

}
