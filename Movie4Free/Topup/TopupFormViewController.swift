//
//  TopupFormViewController.swift
//  Movie4Free
//
//  Created by BURIN TECHAMA on 3/28/2559 BE.
//  Copyright © 2559 JonGT. All rights reserved.
//

import UIKit

class TopupFormViewController: UIViewController {

    @IBOutlet weak var lbl_topup_method: UILabel!
    @IBOutlet weak var img_payment_method: UIImageView!
    @IBOutlet weak var txt_card_code: UITextField!
    @IBOutlet weak var txt_username: UITextField!
    @IBOutlet weak var txt_password: UITextField!
    var topup_method: String!
    var topup_method_img: String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        lbl_topup_method.text = topup_method
        img_payment_method.image = UIImage(named: topup_method_img)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func backPressed(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func checkTopupForm(sender: AnyObject) {
        //check data
        //call service
        //alert
        self.performSegueWithIdentifier("showResult", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showResult") {
            let destinationVC:TopupResultViewController = segue.destinationViewController as! TopupResultViewController
            destinationVC.topup_method = self.topup_method
            destinationVC.topup_method_img = self.topup_method_img
        }
    }
    
    override var preferredFocusedView: UIView?{
        return txt_card_code
    }

}
