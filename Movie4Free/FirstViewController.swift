//
//  FirstViewController.swift
//  Movie4Free
//
//  Created by JonGT on 3/25/2559 BE.
//  Copyright © 2559 JonGT. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
class FirstViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource {
    
//    var menuData:[String] = ["แอคชัน","ตลกเบาสมอง","รักโรแมนติก","สยองขวัญ","การ์ตูน"]
    var arrayMenu : NSMutableArray=NSMutableArray();
    var arrayContent : NSMutableArray=NSMutableArray();

    @IBOutlet weak var menuTable: UITableView!
    @IBOutlet weak var txt_searchText: UITextField!
    @IBOutlet weak var movieListCollection: UICollectionView!
 
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
 getLoginWithUserNameAndPassword("test", password: "test2") { (result, error) in
    
        }
//        Alamofire.request(.GET, getMainURL, parameters: ["foo": "bar"])
//            .responseJSON { response in
//                print(response.request)  // original URL request
//                print(response.response) // URL response
//                print(response.data)     // server data
//                print(response.result)   // result of response serialization
//                
//                if let JSON = response.result.value {
//                    print("JSON: \(JSON)")
//                    let items=JSON["menuItems"] as! NSMutableArray
//                    for item in items {
//                        print("in fot : \(item)")
//
//                        self.arrayMenu.addObject(item)
//
//                    }
//                print("arraymenu: \(self.arrayMenu[1]["title"])")
//                self.menuTable.reloadData()
//
//                }
//        }
        getMenu { (result) in
//            
            print("JSON g: \(result)")
            let items=result["menuItems"] as! NSMutableArray
            for item in items {
                print("in fot : \(item)")
                
                self.arrayMenu.addObject(item)
                
            }
            print("arraymenu: \(self.arrayMenu[1]["title"])")
            getContentWithUrl((self.arrayMenu[0]["targetUrl"] as? String)!, completion: { (result) in
                self.arrayContent = NSMutableArray(array:result["gridContent"]!!["items"] as! NSMutableArray)
                self.movieListCollection.reloadData()

            })
            self.menuTable.reloadData()
        }
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func showLoginView(sender: AnyObject) {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("LoginVC") as! LoginViewController
        let navigationController = UINavigationController(rootViewController: vc) as UINavigationController
        self.presentViewController(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func showTopupView(sender: AnyObject) {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("TopupVC") as! TopupViewController
        let navigationController = UINavigationController(rootViewController: vc) as UINavigationController
        self.presentViewController(navigationController, animated: true, completion: nil)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section==0 {
            return self.arrayMenu.count
        }else if section==1 {
            return 1
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:UITableViewCell = self.menuTable.dequeueReusableCellWithIdentifier("cell")! as UITableViewCell
        
        if indexPath.section == 1 {
            if NSUserDefaults.standardUserDefaults().boolForKey("isLogin") {
                cell.textLabel?.text = "ออกจากระบบ"
            }else{
                cell.textLabel?.text = "เข้าสู่ระบบ"
            }
        }else{
//            print("arraymenu: \(self.arrayMenu[1]["title"])")

            cell.textLabel?.text = self.arrayMenu[indexPath.row]["title"] as? String
        }
        
        cell.textLabel?.textColor = UIColor.lightGrayColor()
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section==1 {
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("LoginVC") as! LoginViewController
            let navigationController = UINavigationController(rootViewController: vc) as UINavigationController
            self.presentViewController(navigationController, animated: true, completion: nil)
        }else{
            getContentWithUrl((self.arrayMenu[indexPath.row]["targetUrl"] as? String)!, completion: { (result) in
                self.arrayContent = NSMutableArray(array:result["gridContent"]!!["items"] as! NSMutableArray)
                self.movieListCollection.reloadData()

            })
        }
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrayContent.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("item", forIndexPath: indexPath) as! MovieCollectionViewCell
        cell.titleView.alpha = 0;

        cell.lbl_head.text=self.arrayContent[indexPath.row]["title"] as! String
        cell.lbl_desc.text=self.arrayContent[indexPath.row]["titleEng"] as! String

        cell.img_poster.af_setImageWithURL(NSURL (string:self.arrayContent[indexPath.row]["imageUrl"] as! String )! )
        cell.titleView_buttom_layout.constant = -60
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didUpdateFocusInContext context: UICollectionViewFocusUpdateContext, withAnimationCoordinator coordinator: UIFocusAnimationCoordinator) {

        if let prevIndexPath = context.previouslyFocusedIndexPath {
            let cell = self.movieListCollection.cellForItemAtIndexPath(prevIndexPath) as! MovieCollectionViewCell
            cell.titleView.alpha = 0;
            cell.titleView_buttom_layout.constant = -60
        }
        
        if let nextIndexPath = context.nextFocusedIndexPath {
            let cell = self.movieListCollection.cellForItemAtIndexPath(nextIndexPath) as! MovieCollectionViewCell
            cell.titleView_buttom_layout.constant = -38
            UIView.animateWithDuration(0.3, animations: { 
                cell.titleView.alpha = 1;
                cell.layoutIfNeeded()
            })
        }
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("MovieDetailVC") as! MovieDetailViewController
        let navigationController = UINavigationController(rootViewController: vc) as UINavigationController
        vc.dictMovie=self.arrayContent[indexPath.row] as! NSDictionary
        self.presentViewController(navigationController, animated: true, completion: nil)
    }

}

