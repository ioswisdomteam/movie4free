//
//  MFConnection.swift
//  Movie4Free
//
//  Created by JonGT on 3/28/2559 BE.
//  Copyright © 2559 JonGT. All rights reserved.
//

import Foundation
import Alamofire
import JWT
//get menu
public func getMenu( completion: (result: AnyObject!) -> Void) {

    Alamofire.request(.POST, getMainURL, parameters: ["foo": "bar"], encoding:.JSON).responseJSON
        { response in switch response.result {
        case .Success(let JSON):
            print("Success with JSON: \(JSON)")
            
            let response = JSON as! NSDictionary
          
            //example if there is an id
            completion(result: response)
        case .Failure(let error):
            print("Request failed with error: \(error)")
            completion(result: "error")
            }
    }
    
}
//getCategory
public func getContentWithUrl(urlString: AnyObject, completion: (result: AnyObject!) -> Void) {
    
    Alamofire.request(.POST, NSURL(string: "\(urlString)")!, parameters: ["foo": "bar"], encoding:.JSON).responseJSON
        { response in switch response.result {
        case .Success(let JSON):
            print("Success Content with JSON: \(JSON)")
            
            let response = JSON 
            
            //example if there is an id
            completion(result: response)
        case .Failure(let error):
            print("Request failed with error: \(error)")
            completion(result: "error")
            }
    }
    
}
//get login
public func getLoginWithUserNameAndPassword(username: AnyObject,password: AnyObject, completion: (result: AnyObject!, error:NSError?) -> Void) {
    
    let jsonObject: [String: AnyObject] = [
        
        "username": username,
        "password": password,
        "deviceId": UIDevice.currentDevice().identifierForVendor!.UUIDString,
        "deviceName": UIDevice.currentDevice().name,
        "nonce": "123123123",
        "timestamp": Timestamp
    ]
    
    let valid = NSJSONSerialization.isValidJSONObject(jsonObject) // true
    
//    JWT.encode(jsonObject, algorithm: .HS256(secretkey))
    let params = JWT.encode(jsonObject, algorithm: .HS256(secretkey)) as String
    print("JWT LOGIN : \(params)")

    Alamofire.request(.POST, getLoginURL, parameters: ["data":params], encoding:.JSON).responseJSON
        { response in switch response.result {
            
        case .Success(let JSON):
            print("Success with JSON: \(JSON)")
            
            let response = JSON as! NSDictionary
            
            //example if there is an id
            completion(result: response, error: nil)
        case .Failure(let error):
            print("Request failed with error: \(error)")
            completion(result: "error", error:error as NSError)
            }
    }
    
}

//get forgot password
public func getForgorPasswordWithUserName(username: AnyObject, completion: (result: AnyObject!, error:NSError?) -> Void) {
    
    let jsonObject: [String: AnyObject] = [
        
        "username": username,
        "deviceId": UIDevice.currentDevice().identifierForVendor!.UUIDString,
        "deviceName": UIDevice.currentDevice().name,
        "nonce": "123123123",
        "timestamp": Timestamp
    ]
    
    let valid = NSJSONSerialization.isValidJSONObject(jsonObject) // true
    
    //    JWT.encode(jsonObject, algorithm: .HS256(secretkey))
    let params = JWT.encode(jsonObject, algorithm: .HS256(secretkey)) as String
    print("JWT LOGIN : \(params)")
    
    Alamofire.request(.POST, getForgotURL, parameters: ["data":params], encoding:.JSON).responseJSON
        { response in switch response.result {
            
        case .Success(let JSON):
            print("Success with JSON: \(JSON)")
            
            let response = JSON as! NSDictionary
            
            //example if there is an id
            completion(result: response, error: nil)
        case .Failure(let error):
            print("Request failed with error: \(error)")
            completion(result: "error", error:error as NSError)
            }
    }
    
}

func convertJWT(payload: AnyObject,completion: (result: AnyObject!) -> Void) {
    getLoginWithUserNameAndPassword("test", password: "asdf") { (result) in
        
    }
    
}
