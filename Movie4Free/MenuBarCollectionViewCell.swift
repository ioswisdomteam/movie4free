//
//  MenuBarCollectionViewCell.swift
//  Movie4Free
//
//  Created by BURIN TECHAMA on 3/30/2559 BE.
//  Copyright © 2559 JonGT. All rights reserved.
//

import UIKit

class MenuBarCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var img_bg: UIImageView!
}
