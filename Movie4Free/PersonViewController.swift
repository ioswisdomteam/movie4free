//
//  PersonViewController.swift
//  Movie4Free
//
//  Created by BURIN TECHAMA on 3/31/2559 BE.
//  Copyright © 2559 JonGT. All rights reserved.
//

import UIKit

class PersonViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {

    @IBOutlet weak var img_person: UIImageView!
    @IBOutlet weak var lbl_position: UILabel!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_name_eng: UILabel!
    @IBOutlet weak var lbl_location: UILabel!
    @IBOutlet weak var txv_detail: UITextView!
    @IBOutlet weak var page_scroll_view: UIScrollView!
    @IBOutlet weak var videoCollection: UICollectionView!
    @IBOutlet weak var relatedMovieCollection: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidLayoutSubviews() {
        self.page_scroll_view.contentSize = CGSizeMake(1920, 1280)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == 0 {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("video", forIndexPath: indexPath) as! VideoCollectionViewCell
            return cell
        }else if collectionView.tag == 1 {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("RelatedMovie", forIndexPath: indexPath) as! RelatedMovieCollectionViewCell
            return cell
        }
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as UICollectionViewCell
        return cell
    }
    

}
