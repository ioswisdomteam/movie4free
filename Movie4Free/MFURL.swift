//
//  MFURL.swift
//  Movie4Free
//
//  Created by JonGT on 3/25/2559 BE.
//  Copyright © 2559 JonGT. All rights reserved.
//

import Foundation
public let secretkey="uB2Ph3YVzTjXjKhZ58tv"
public let getMainURL="http://45.64.184.39:8080/v1/store/menu"
public let getBannerURL="http://45.64.184.39:8080/v1/store/banner"
public let getRegisterURL="http://45.64.184.39:8080/v1/user/register"
public let getLoginURL="http://45.64.184.39:8080/v1/user/auth"
public let getForgotURL="http://45.64.184.39:8080/v1/user/forgotpassword"
public let getPaymentURL="http://45.64.184.39:8080/v1/payment/cashcard"
public let getMovieLinkURL="http://45.64.184.39:8080/v1/movies/streamlinks/1"
public let getChkSubsriptionURL="http://45.64.184.39:8080/v1/user/isSubscription"
public let getSearchMovieURL="http://45.64.184.39:8080/v1/movie/search"
public var Timestamp: NSTimeInterval {
    return NSDate().timeIntervalSince1970 * 1000
}