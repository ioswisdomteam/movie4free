//
//  ProfileCollectionViewCell.swift
//  Movie4Free
//
//  Created by BURIN TECHAMA on 3/30/2559 BE.
//  Copyright © 2559 JonGT. All rights reserved.
//

import UIKit

class ProfileCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var img_profile: UIImageView!
    @IBOutlet weak var lbl_name: UILabel!
    
    override func drawRect(rect: CGRect) {
        img_profile.adjustsImageWhenAncestorFocused = true
        img_profile.clipsToBounds = false
    }
    
}
