//
//  SuggestCollectionViewCell.swift
//  Movie4Free
//
//  Created by BURIN TECHAMA on 3/30/2559 BE.
//  Copyright © 2559 JonGT. All rights reserved.
//

import UIKit

class SuggestCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var img_suggest: UIImageView!
    
    override func drawRect(rect: CGRect) {
        img_suggest.adjustsImageWhenAncestorFocused = true
        img_suggest.clipsToBounds = false
    }
}
