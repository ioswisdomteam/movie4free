//
//  RelatedMovieCollectionViewCell.swift
//  Movie4Free
//
//  Created by BURIN TECHAMA on 3/31/2559 BE.
//  Copyright © 2559 JonGT. All rights reserved.
//

import UIKit

class RelatedMovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var img_related_movie: UIImageView!
    
    override func drawRect(rect: CGRect) {
        img_related_movie.adjustsImageWhenAncestorFocused = true
        img_related_movie.clipsToBounds = false
    }
}
