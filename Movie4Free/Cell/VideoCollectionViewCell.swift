//
//  VideoCollectionViewCell.swift
//  Movie4Free
//
//  Created by BURIN TECHAMA on 3/31/2559 BE.
//  Copyright © 2559 JonGT. All rights reserved.
//

import UIKit

class VideoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var img_video: UIImageView!
    
    override func drawRect(rect: CGRect) {
        img_video.adjustsImageWhenAncestorFocused = true
        img_video.clipsToBounds = false
    }

    
}
