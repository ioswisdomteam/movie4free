//
//  MovieCollectionViewCell.swift
//  Movie4Free
//
//  Created by BURIN TECHAMA on 3/29/2559 BE.
//  Copyright © 2559 JonGT. All rights reserved.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
    
    var imageView:UIImageView!
    
    @IBOutlet weak var img_poster: UIImageView!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var lbl_head: UILabel!
    @IBOutlet weak var lbl_desc: UILabel!
    @IBOutlet weak var titleView_buttom_layout: NSLayoutConstraint!
    
    override func drawRect(rect: CGRect) {
        img_poster.adjustsImageWhenAncestorFocused = true
        img_poster.clipsToBounds = false
        img_poster.backgroundColor = UIColor.blueColor()
    }
    
}
