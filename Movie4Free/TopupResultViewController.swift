//
//  TopupResultViewController.swift
//  Movie4Free
//
//  Created by BURIN TECHAMA on 3/29/2559 BE.
//  Copyright © 2559 JonGT. All rights reserved.
//

import UIKit

class TopupResultViewController: UIViewController {

    @IBOutlet weak var lbl_topup_method: UILabel!
    @IBOutlet weak var img_topup_method: UIImageView!
    @IBOutlet weak var lbl_topup_detail: UILabel!
    var topup_method: String!
    var topup_method_img: String!
    @IBOutlet weak var btn_go_main: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        lbl_topup_method.text = topup_method
        img_topup_method.image = UIImage(named: topup_method_img)
        lbl_topup_detail.text = "คุณได้ทำการเติมเงินเข้าระบบ เป็นจำนวน 500 บาท สามารถชมภาพยนตร์ได้ 30 วัน"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func goBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func gotoMain(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    override var preferredFocusedView: UIView?{
        return btn_go_main
    }

}
