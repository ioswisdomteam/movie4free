//
//  MovieDetailViewController.swift
//  Movie4Free
//
//  Created by BURIN TECHAMA on 3/30/2559 BE.
//  Copyright © 2559 JonGT. All rights reserved.
//

import UIKit
import QuartzCore

class MovieDetailViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    var dictMovie:NSDictionary = NSDictionary();
    var movieDetailDict:NSDictionary = NSDictionary();

    var arrayContent:NSMutableArray = NSMutableArray();
    var peopleArray:NSMutableArray = NSMutableArray();
    var relatedMovieArray:NSMutableArray = NSMutableArray();

    @IBOutlet weak var img_bg_top: UIImageView!
    @IBOutlet weak var img_movie: UIImageView!
    @IBOutlet weak var menu_bar_collection: UICollectionView!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_min: UILabel!
    @IBOutlet weak var lbl_type: UILabel!
    @IBOutlet weak var lbl_date: UILabel!
    @IBOutlet weak var txv_desc: UITextView!
    @IBOutlet weak var page_scroll_view: UIScrollView!
    var menuBarList:[String] = ["หนังตัวอย่าง","Play","เติมเงินเข้าระบบ"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("dict \(dictMovie)")
        getContentWithUrl((dictMovie["targetUrl"] as? String)!, completion: { (result) in
            print("arrayContent \(self.arrayContent)")

            for var i = 0; i <= self.arrayContent.count; i += 1 {
                if self.arrayContent[i]["type"] as! String=="movieDetail"{
                    self.movieDetailDict=self.arrayContent[i] as! NSDictionary
                }else if self.arrayContent[i]["type"] as! String=="people"{
                    self.peopleArray.addObject(self.arrayContent[i])
                    
                }else if self.arrayContent[i]["type"] as! String=="relatedMovie"{
                    
                }
                
            }
            
        })
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        self.page_scroll_view.contentSize = CGSizeMake(1920, 1880)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 0 {
            return 3
        }else if collectionView.tag == 1 {
            return 2
        }else if collectionView.tag == 2 {
            return 3
        }else if collectionView.tag == 3 {
            return 4
        }else if collectionView.tag == 4 {
            return 5
        }else {
            return 0
        }
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == 0 {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("menu", forIndexPath: indexPath) as! MenuBarCollectionViewCell
            cell.img_bg.alpha = 0
            cell.lbl_title.text = menuBarList[indexPath.row]
            return cell
        }else if collectionView.tag == 1 {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("profile", forIndexPath: indexPath) as! ProfileCollectionViewCell
            cell.img_profile.layer.cornerRadius = 40
            cell.img_profile.clipsToBounds = true
            return cell
        }else if collectionView.tag == 2 {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("profile", forIndexPath: indexPath) as! ProfileCollectionViewCell
            return cell
        }else if collectionView.tag == 3 {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("profile", forIndexPath: indexPath) as! ProfileCollectionViewCell
            return cell
        }else if collectionView.tag == 4 {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("suggest", forIndexPath: indexPath) as! SuggestCollectionViewCell
            return cell
        }
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as UICollectionViewCell
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didUpdateFocusInContext context: UICollectionViewFocusUpdateContext, withAnimationCoordinator coordinator: UIFocusAnimationCoordinator) {
        
        if collectionView.tag == 0 {
            if let prevIndexPath = context.previouslyFocusedIndexPath {
                let cell = self.menu_bar_collection.cellForItemAtIndexPath(prevIndexPath) as! MenuBarCollectionViewCell
                cell.img_bg.alpha = 0;
            }
            
            if let nextIndexPath = context.nextFocusedIndexPath {
                let cell = self.menu_bar_collection.cellForItemAtIndexPath(nextIndexPath) as! MenuBarCollectionViewCell
                UIView.animateWithDuration(0.3, animations: {
                    cell.img_bg.alpha = 1;
                })
            }
        }
        
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if collectionView.tag == 0 {
            if indexPath.row == 2 {
                let vc = self.storyboard?.instantiateViewControllerWithIdentifier("TopupVC") as! TopupViewController
                let navigationController = UINavigationController(rootViewController: vc) as UINavigationController
                self.presentViewController(navigationController, animated: true, completion: nil)
            }
        }
        if collectionView.tag == 1 || collectionView.tag == 2 || collectionView.tag == 3 {
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("PersonVC") as! PersonViewController
            let navigationController = UINavigationController(rootViewController: vc) as UINavigationController
            self.presentViewController(navigationController, animated: true, completion: nil)
        }
    }
    
    override var preferredFocusedView: UIView?{
        return self.menu_bar_collection
    }
    

}
